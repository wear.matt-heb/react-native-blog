import React, {useState} from 'react';
import {View, Text, StyleSheet, TextInput, Button} from 'react-native';

const BlogPostForm = ({onSubmit, blogPost}) => {    

    const [title, setTitle] = useState(blogPost.title);
    const [content, setContent] = useState(blogPost.content); 

    return (
        <View>
            <Text style={styles.label}>Enter Title:</Text>
            <TextInput style={styles.input} value={title} onChangeText={(text) => setTitle(text)} />
            <Text style={styles.label}>Enter Content:</Text>
            <TextInput style={styles.input} value={content} onChangeText={(content) => setContent(content)} /> 
            <Button title="Save" onPress={() => onSubmit(title, content)} />
        </View>
    );
};

const styles = StyleSheet.create({
    input: {
        fontSize: 18,
        borderWidth: 1,
        borderColor: 'black'
    },
    label: {
        fontSize: 20, 
        marginBottom: 10
    }
});

export default BlogPostForm; 