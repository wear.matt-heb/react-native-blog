import createDataContext from './createDataContext';
import jsonserver from '../api/jsonserver';


const blogReducer = (state, action) => {
    switch(action.type) {
        case 'get_blogposts':
            return action.payload;
        case 'edit_blogpost':
            return state.map((blogPost) => {
                return blogPost.id === action.payload.id ? action.payload : blogPost })
        case 'delete_blogpost':
            return state.filter((blogPost) => blogPost.id !== action.payload);   
        default:
            return state;
    }
};

const getBlogPosts = dispatch => {
    return async () => {
        const response = await jsonserver.get('/blogposts');

        console.log(response.data);
        dispatch({type: 'get_blogposts', payload: response.data});
    };
};

const addBlogPost = (dispatch) => {
    return async (title, content, callback) => {
        const response = await jsonserver.post('/blogposts', {title, content});

        callback();
    };
};

const editBlogPost = (dispatch) => {
    return async (id, title, content, callback) => {
        await jsonserver.put(`/blogposts/${id}`, {title, content});
        
        dispatch({type: 'edit_blogpost', payload: {id, title, content}})
        
        callback();
    };
};

const deleteBlogPost = (dispatch) => {
    return async (id) => {
        const response = await jsonserver.delete(`/blogposts/${id}`);
        
        dispatch({type: 'delete_blogpost', payload: id});
    };
};

export const { Context, Provider } = createDataContext(
    blogReducer, 
    {
        getBlogPosts,
        addBlogPost, 
        deleteBlogPost, 
        editBlogPost
    }, 
    []
);